# animated-notification
A small animation was developed simulating the movement of a bell when receiving a notification with CSS.

![ring-notification](img/ring-1.png);

![animated-ring-notification](img/ring-2.png);

![animated-ring-notification](img/ring-3.png);